<?php
use PHPUnit\Framework\TestCase;

$root=realpath($_SERVER['DOCUMENT_ROOT']);
require($root.'/config/database.php');

/**
 * A simple PHPUnit test for Insert,Update,Select with Mock
 */
class DatabaseClassTest extends TestCase 
{
    /** @test */
    public function is_select_works_properly()
    {

        $mockRepo = $this->createMock(DatabaseClass::class);
        
        $mockUserArray = [
            ['userId'=>1, 'name'=>"Babak"]
        ];

        $mockRepo->expects($this->once())->method('Select')->willReturn($mockUserArray);

        $users = $mockRepo->Select();

        $this->assertEquals("Babak", $users[0]['name']);

    }
    /** @test */
    public function is_insert_works_properly()
    {
        $mockRepo = $this->createMock(DatabaseClass::class);
        
        $mockUserArray = [
            ['userId'=>1, 'name'=>"Babak", "year_Of_Birth"=>"1979"]
        ];

        $mockRepo->expects($this->once())->method('Insert')->willReturn($mockUserArray);

        $users = $mockRepo->Insert();

        
        $this->assertEquals("Babak", $users[0]['name']);
    }
    /** @test */
    public function is_update_works_properly()
    {
        $mockRepo = $this->createMock(DatabaseClass::class);
        
        $mockUserArray = [
            ['userId'=>1, 'name'=>"Arvin", "year_Of_Birth"=>"2018"]
        ];

        $mockRepo->expects($this->once())->method('Update')->willReturn($mockUserArray);

        $users = $mockRepo->Update();

        $this->assertEquals("Arvin", $users[0]['name']);
    }
}
# Babak Khayatian
## _CH-Aviation Task 1_

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This task designed and developed according to the API and you need to install POSTMAN.

After the code clone, please use the Main branch and use composer install and then copy .env.example to .env for testing with actual DB. If you want to don't connect to the actual DB and test the code. Please use PHPUnit.

```sh
composer install

copy .env.example .env
```

I developed a simple PHPUnit mock test for CRUD (Except delete). You can use the following command to run the test:

```sh
 ./vendor/bin/phpunit
 ```
 
You can download the sample DB as follows:

[sampleDB]





   [sampleDB]: <https://file.io/hENVEckcSCNa>

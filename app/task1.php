<?php
declare(strict_types = 1);
$root=realpath($_SERVER['DOCUMENT_ROOT']);
include_once($root.'/config/headers.php');
include_once($root.'/config/database.php');

$req=(json_decode(file_get_contents("php://input")));
if (empty($req) && !isset($_GET['userId']))
    header("HTTP/1.1 400 Bad request");

/**
 * Connecting to database with PDOConnection
 */
$conn = new DatabaseClass;
$conn->PDOConnection();

/**
 * If @userId sent through URL by an API
 * If $_POST['name'] sent through an API, it will update the existing row
 * If $_POST['name'] didn't send through an API, it will fetch the row according to the @userId
 */
if (isset($_GET['userId']) && !empty($_GET['userId']))
{
    if (isset($req->name) && (!empty($req->name)))
    {
        //If $_POST['yearOfBirth'] sent it by typo error
        if (!isset($req->yearOfBirth))
        {
            echo json_encode(array (
                "data"=> null,
                "Message"=> "The column name is wrong"
            ));
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        } else {
            $updatedAt=date('Y-m-d');
            $stmtUpdate = $conn->Update("UPDATE users SET name = :name, year_of_birth = :year_of_birth, updatedAt = :updatedAt WHERE userId = :id", ['name'=>$req->name, 'year_of_birth'=>$req->yearOfBirth, 'updatedAt'=>$updatedAt, 'id'=>$_GET['userId']]);
            if ((int)json_encode($stmtUpdate['result']) > 0)
            {
                echo json_encode(array (
                    "data"=> null,
                    "Message"=> "Row Successfully Updated"
                ));
                header($_SERVER['SERVER_PROTOCOL'] . ' 200 Successful', true, 200);
            }
            if ((int)json_encode($stmtUpdate['result']) === 0)
            {
                echo json_encode(array (
                    "data"=> null,
                    "Message"=> "There's no row update"
                ));
                header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
            } 
        }
    } else {
        $stmtSelect = $conn->Select("SELECT * FROM users WHERE userId = :id", ["id"=>$_GET['userId']]); 
        $rowCount = json_encode(count($stmtSelect));
        if ($rowCount>0)
        {
            echo json_encode(array (
                "data"=> $stmtSelect,
                "Message"=> "Successful"
            ));
            header($_SERVER['SERVER_PROTOCOL'] . ' 200 Successful', true, 200);
        } 
        if ($rowCount==0)
        {
            echo json_encode(array (
                "data"=> null,
                "Message"=> "No record has been found"
            ));
            header($_SERVER['SERVER_PROTOCOL'] . ' 404 No record has been found', true, 404);
        }
    }
}
/**
 * If @userId didn't send or sent it without value through URL by an API and
 * If $_POST['name'] didn't send through an API, it will insert a new record
 */
if (isset($req->name) && (!isset($_GET['userId']) || empty($_GET['userId'])) && !empty($req->name)) {
    //If $_POST['yearOfBirth'] sent it by typo error
    if (!isset($req->yearOfBirth))
    {
        echo json_encode(array (
            "data"=> null,
            "Message"=> "The column name is wrong"
        ));
        header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
    } else {
        $createdAt=date('Y-m-d');
        $stmtInsert=$conn->Insert("INSERT into users (name,year_of_birth,createdAt) VALUES (:name, :yearOfBirth, :createdAt)", ['name'=>$req->name, 'yearOfBirth'=>$req->yearOfBirth, 'createdAt'=>$createdAt]);
        if ((int)json_encode($stmtInsert['result']) > 0)
        {
            echo json_encode(array (
                "data"=> null,
                "Message"=> "Row Successfully Inserted"
            ));
            header($_SERVER['SERVER_PROTOCOL'] . ' 201 Created Successfully', true, 201);
        }
        if ((int)json_encode($stmtInsert['result']) === 0)
        {
            echo json_encode(array (
                "data"=> null,
                "Message"=> "No record inserted"
            ));
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        }   
    }
}
/**
 * If $_POST['name'] sent it with typo error
 */
if (!isset($req->name) && !isset($_GET['userId']))
{
    echo json_encode(array (
        "data"=> null,
        "Message"=> "The column name is wrong"
    ));
    header($_SERVER['SERVER_PROTOCOL'] . ' 400 Bad Request', true, 400);
}
?>
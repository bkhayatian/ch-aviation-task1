<?php
declare(strict_types=1);

$root = realpath($_SERVER['DOCUMENT_ROOT']);
require_once($root.'/config/dotenv.php');

use DevCoder\DotEnv;

(new DotEnv($root.'/.env'))->load();

class DatabaseClass
{

    private $connectionType = NULL;
    private $connection = NULL;
    protected $host;
    protected $username;
    protected $password;
    protected $database;
    protected $dbDriver;

    public function __construct()
    {
        $this->host = $_ENV['MYSQL_HOST'];
        $this->username = $_ENV['MYSQL_USERNAME'];
        $this->password = $_ENV['MYSQL_PASSWORD'];
        $this->database = $_ENV['MYSQL_DATABASE'];
        $this->dbDriver = $_ENV['MYSQL_DRIVER'];
    }

    public function mysqliConnection()
    {
        try {
            $this->connection = new mysqli($this->host, $this->username, $this->password);
            $this->connectionType = 'MySQLi';
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function PDOConnection()
    {
        try {
            $this->connection = new PDO("mysql:host=".$this->host.";dbname=".$this->database, $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connectionType = 'PDO';
        } catch (ExcepPDOExceptiontion $e) {
            throw new PDOException($e->getMessage(), (int) $e->getCode());
        }
    }

    private function executeStatement(string $statement = NULL, array $parameters = [])
    {
        switch ($this->connectionType) {
            case "MySQLi":
                return "MySQLi";
                break;
            case "PDO":
                try {
                    $stmt = $this->connection->prepare($statement);
                    $stmt->execute($parameters);
                    return $stmt;
                    break;
                } catch (PDOException $e) {
                    throw new PDOException ($e->getMessage(), (int) $e->getCode());
                }
                
            }
    }
    public function Insert(string $statement = NULL, array $parameters = []): array
    {      
        try {
            $stmt = $this->executeStatement($statement, $parameters);
            return ["result"=>1];
        } catch (PDOException $e) {
            throw new PDOException ($e->getMessage(), (int) $e->getCode());
        }
    }

    public function Update(string $statement = NULL, array $parameters = []): array
    {
        try {
            $stmt = $this->executeStatement($statement, $parameters);
            if ($stmt->rowCount() > 0)
            {
                return ["result"=>$stmt->rowCount()];
            } else {
                return ["result"=>$stmt->rowCount()];
            }
        } catch (PDOException $e) {
            throw new PDOException ($e->getMessage(), (int) $e->getCode());
        }
    }

    public function Select(string $statement = NULL, array $parameters = []): array
    {
        try {
            $stmt = $this->executeStatement($statement, $parameters);
            $result = $stmt->fetchAll();
            return $result;
        } catch (PDOException $e) {
            throw new PDOException ($e->getMessage(), (int) $e->getCode());
        }
    }
}
?>